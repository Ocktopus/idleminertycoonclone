﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class is the abstract class that factor all common code from all buildings.

public abstract class Building : MonoBehaviour {

	protected double levelUpCostRate;
	protected double actionRate = 0; 
	protected double stock = 0;
	protected int level = 1;
	protected double levelUpCost;
	protected double temp;

	public void UpdateText(){
		if(stock >= 1000){
			temp = stock/1000;
			gameObject.transform.GetChild(0).GetComponent<TextMesh>().text = ""+temp.ToString("F1")+"K";
		}
		else{
			gameObject.transform.GetChild(0).GetComponent<TextMesh>().text = ""+stock;	
		}
	}

	public double GetActionRate(){
		return actionRate;
	}
	public double GetStock(){
		return stock;
	}

	//this method is used to pick up some coal from a building.
	public double PickUp(double qty){
		double pick = stock - qty;
		if(pick >= 0){
			stock -= qty;
			return qty;
		}
		else{
			pick = stock;
			stock = 0;
			return pick;
		}
	}

	//this method is the one called when the user want to level up a building.
	public void LevelUp(){
		if(gameObject.transform.parent.GetComponent<World>().GetCredits() >= levelUpCost){
			gameObject.transform.parent.GetComponent<World>().GetWarehouse().PickUp(levelUpCost);
			level++;
			levelUpCost = GetLevelUpCostRate()*level;
			actionRate*= level;
			gameObject.transform.GetChild(2).GetComponent<TextMesh>().text = "Level "+level;
			if(levelUpCost >= 1000){
				temp = levelUpCost/1000;
				gameObject.transform.GetChild(1).GetChild(1).GetComponent<TextMesh>().text = ""+temp.ToString("F1")+"K";
			}
			else{
				gameObject.transform.GetChild(1).GetChild(1).GetComponent<TextMesh>().text = ""+levelUpCost;
			}
		}
		else{
			Debug.Log("Not Enough credits !");
		}
	}

	public abstract double GetLevelUpCostRate();
}
