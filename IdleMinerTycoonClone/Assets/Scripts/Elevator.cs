﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : Building {

	private new const double levelUpCostRate = 500;

	void Start() {
		actionRate = 3;
		levelUpCost = levelUpCostRate*level;
		gameObject.transform.GetChild(1).GetChild(1).GetComponent<TextMesh>().text = ""+levelUpCost;
	}
	void FixedUpdate(){
		if(Time.fixedTime%1 == 0 && Time.fixedTime>1){
			for(int i = 0; i < gameObject.transform.parent.GetComponent<World>().GetMines().Count; i++){
				stock += gameObject.transform.parent.GetComponent<World>().GetMines()[i].PickUp(actionRate);
			}
		}
	}
	public override double GetLevelUpCostRate(){
		return levelUpCostRate;
	}
}
