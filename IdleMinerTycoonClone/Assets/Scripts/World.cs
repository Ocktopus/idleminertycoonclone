﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {

	public GameObject elevator_Prefab;
	public GameObject mine_Prefab;				// These attributes are all prefabs that I will need
	public GameObject warehouse_Prefab;
	public GameObject addButton_Prefab;


	[SerializeField]
	private Elevator elevator;
	[SerializeField]							//These attribut are Serialized to be seen in the editor
	private List<Mine> mines = new List<Mine>();
	[SerializeField]
	private Warehouse warehouse;
	private GameObject addButton;
	private double credits = 0;
	private double price = 10;
	private double temp;


	// In this Start, I create all element of the game
	void Start () {
		GameObject creator;
		creator = Instantiate(elevator_Prefab, new Vector3(-10,1,0), Quaternion.identity,gameObject.transform);
		elevator = creator.GetComponent<Elevator>();
		creator = Instantiate(warehouse_Prefab, new Vector3(10,1,0), Quaternion.identity,gameObject.transform);
		warehouse = creator.GetComponent<Warehouse>();
		creator = Instantiate(mine_Prefab, new Vector3(0,-4,0), Quaternion.identity,gameObject.transform);
		Mine m = creator.GetComponent<Mine>();
		mines.Add(m);
		m.CreateMine(mines.Count);
		addButton = Instantiate(addButton_Prefab, new Vector3(0,-10,0), Quaternion.identity, gameObject.transform);
		addButton.transform.GetChild(0).GetComponent<TextMesh>().text = ""+mines.Count*price;
	}
	
	// In this update, I update all visual aspect of the game.
	void Update () {
		credits = warehouse.GetStock();
		UpdateText();
	}

	//This method is the one used to update visuals that need to be updated often.
	void UpdateText(){
		if(credits >= 1000){
			temp = credits/1000;
			gameObject.transform.Find("Credits").GetComponent<TextMesh>().text = ""+temp.ToString("F1")+"K";
		}
		else{
			gameObject.transform.Find("Credits").GetComponent<TextMesh>().text = ""+credits;
		}
		elevator.UpdateText();
		warehouse.UpdateText();
		for(int i = 0; i < mines.Count; i++){
			mines[i].UpdateText();
		}
	}

	public List<Mine> GetMines(){
		return mines;
	}

	public Elevator GetElevator(){
		return elevator;
	}

	public Warehouse GetWarehouse(){
		return warehouse;
	}

	public double GetCredits(){
		return credits;
	}


	//This method is used to add a new mine in a new layer.
	public void AddMine(){
		if(credits >= mines.Count*price){
			warehouse.PickUp(mines.Count*price);
			GameObject creator = Instantiate(mine_Prefab, new Vector3(mines[mines.Count-1].transform.position.x, mines[mines.Count-1].transform.position.y - 5, mines[mines.Count-1].transform.position.z), Quaternion.identity, gameObject.transform);
			Mine m = creator.GetComponent<Mine>();
			mines.Add(m);
			m.CreateMine(mines.Count);
			creator.GetComponent<TextMesh>().text = "Mine "+mines.Count;
			addButton.transform.position = new Vector3(addButton.transform.position.x, addButton.transform.position.y - 5, addButton.transform.position.z);
			addButton.transform.GetChild(0).GetComponent<TextMesh>().text = ""+mines.Count*price;
		}
		else{
			Debug.Log("You don't have enough credits");
		}
	}
}
