﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : Building {
	private new const double levelUpCostRate = 100;
	private const double baseIncome = 10;
	void FixedUpdate(){
		if(Time.fixedTime%1 == 0 && Time.fixedTime>1){
			stock += actionRate;
		}
	}
	public void CreateMine(int index){
		actionRate = index*baseIncome;
		levelUpCost = levelUpCostRate*level*index;
		gameObject.transform.GetChild(1).GetChild(1).GetComponent<TextMesh>().text = ""+levelUpCost;
	}
	public override double GetLevelUpCostRate(){
		return levelUpCostRate;
	}
}
