﻿using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine;

public class LevelButton : Button {

	public override void OnPointerClick(PointerEventData eventData)
    {
		gameObject.transform.parent.GetComponent<Building>().LevelUp();
    }
}
