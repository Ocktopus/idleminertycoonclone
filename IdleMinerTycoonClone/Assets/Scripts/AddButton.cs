﻿using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine;

public class AddButton : Button {
	public override void OnPointerClick(PointerEventData eventData)
    {
		gameObject.transform.parent.parent.GetComponent<World>().AddMine();
    }
}
