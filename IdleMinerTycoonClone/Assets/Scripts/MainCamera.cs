﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour {

	public float CameraSpeed = 0.1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var viewportCoord = Camera.main.ScreenToViewportPoint(Input.mousePosition);
 		if (viewportCoord.x < 0.05) {
    		transform.position = new Vector3(gameObject.transform.position.x - CameraSpeed,gameObject.transform.position.y,gameObject.transform.position.z);
 		}
 		if (viewportCoord.x > 0.95) {
     		transform.position = new Vector3(gameObject.transform.position.x + CameraSpeed,gameObject.transform.position.y,gameObject.transform.position.z);
 		}
		 if (viewportCoord.y < 0.05) {
    		transform.position = new Vector3(gameObject.transform.position.x,gameObject.transform.position.y - CameraSpeed,gameObject.transform.position.z);
 		}
 		if (viewportCoord.y > 0.95) {
     		transform.position = new Vector3(gameObject.transform.position.x,gameObject.transform.position.y + CameraSpeed,gameObject.transform.position.z);
 		}	
	}
}
