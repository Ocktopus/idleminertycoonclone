﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warehouse : Building {
	private new const double levelUpCostRate = 50;

	void Start() {
		actionRate = 1;
		levelUpCost = levelUpCostRate*level;
		gameObject.transform.GetChild(1).GetChild(1).GetComponent<TextMesh>().text = ""+levelUpCost;
	}
	
	void FixedUpdate(){
		if(Time.fixedTime%1 == 0){
			stock += gameObject.transform.parent.GetComponent<World>().GetElevator().PickUp(actionRate);
		}
	}
	public override double GetLevelUpCostRate(){
		return levelUpCostRate;
	}
}
